#include <string>
#include <vector>
#include <iostream>

using namespace std;


int main(){
	vector <char> vec1 {'c','h','a','r','a','c','t','e','r'};
	string str1 = "hello";
	auto str2 = str1 + "people";
	
	cout << "string 1 is: " << str1 <<endl;
	cout << "string 2 is: " << str2 <<endl;
	cout << "string 1 has " << str1.size() << " characters" << endl;
	cout << "string 2 has " << str2.size() << " characters" << endl;
	cout << "the 5th character is " << str2[4] << endl;
	
	vec1.pop_back();
	vec1.push_back('r');
	vec1.push_back('s');
	
	for (int i=0; i < vec1.size(); i++)
	{
		cout << vec1[i] << " ";
	}
	
	return 0;
}
